/*
*  create a list of tasks, check event, add task, delete task
* */
let items = []
const form = document.querySelector('form')
const entry = document.getElementsByClassName('tasks')
const deleteTaskButton = document.querySelector('button')
// const addTaskButton
const log = document.getElementById('log')

/**
 * method handles adding task to list and
 * @param event
 */
function logTodoItemSubmission(event) {
    log.textContent = 'Task Added!  Time: ${event.timeStamp}'
    event.preventDefault()
}

form.addEventListener('submit', logTodoItemSubmission)