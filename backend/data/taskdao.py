from flask_restful import fields, marshal_with

resource_fields = {
    'task': fields.String,
    'uri': fields.Url('todo_ep')
}


class TaskDao(object):
    def __init__(self, task_id, task):
        self.task_id = task_id
        self.task = task
        self.status = 'active'
