from app import db
from sqlalchemy.dialects.postgresql import JSON


class TodoData(db.Model):
    __tablename__ = 'todo_data'

    task_id = db.Column(db.Integer, primary_key=True)
    task_name = db.Column(db.String())
    created_timestamp = db.Column(db.TIMESTAMP)
    is_done = db.Column(db.Boolean)
    completed_timestamp = db.Column(db.Boolean)

    def __init__(self, task_name, created_timestamp, is_done, completed_timestamp):
        self.task_name = task_name
        self.created_timestamp = created_timestamp
        self.is_done = is_done
        self.completed_timestamp = completed_timestamp

    def __repr__(self):
        return f'<id {self.task_id}>'
