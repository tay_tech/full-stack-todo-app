import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_DATABASE_HOST = os.environ['DATABASE_HOST']
    SQLALCHEMY_DATABASE_NAME = os.environ['DATABASE_NAME']
    SQLALCHEMY_DATABASE_USER = os.environ['DATABASE_USER']
    SQLALCHEMY_DATABASE_PWD = os.environ['DATABASE_PASSWORD']


class TestingConfig(Config):
    TESTING = True
