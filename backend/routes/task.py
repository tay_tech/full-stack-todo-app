from datetime import datetime
from flask import Flask
from flask_restful import reqparse, abort, Resource

tasks = {
    'task1': {'task': 'build an API'},
    'task2': {'task': '?????'},
    'task3': {'task': 'profit!'},
}
parser = reqparse.RequestParser()
parser.add_argument('task')


def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


class Task(Resource):
    # def __init__(self, task_id):
    #     self.task_id = task_id

    def get(self, task_id):
        return tasks[task_id]

    def update(self, task_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        tasks[task_id] = task
        return task, 201

    def delete(self, task_id):
        del tasks[task_id]
        return '', 204


class TaskList(Resource):
    def get(self):
        return tasks

    def post(self):
        args = parser.parse_args()
        task_id = int(max(tasks.keys()).lstrip('task')) + 1
        task_id = 'task%i' % task_id
        tasks[task_id] = {'task': args['task']}
        return tasks[task_id], 201
