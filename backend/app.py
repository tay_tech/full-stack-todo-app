from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Resource, Api
import os
from requests.exceptions import HTTPError
import requests
from sqlalchemy import create_engine
from routes.task import Task, TaskList

app = Flask(__name__)
api = Api(app)
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db_user = os.environ['DATABASE_USER']
db_pwd = os.environ['DATABASE_PASSWORD']
db_name = os.environ['DATABASE_NAME']
db = SQLAlchemy(app)
migrate = Migrate(app, db)
engine = create_engine(f'postgresql+psycopg2://{db_user}:{db_pwd}@localhost/{db_name}')
engine.connect()


api.add_resource(TaskList, '/tasks')
api.add_resource(Task, '/tasks/<task_id>')

if __name__ == '__main__':
    app.run()
